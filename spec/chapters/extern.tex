\chapter{External Interface}

The external interface of the TCU allows remote tiles to manipulate its state. First of all, the
available TCU features can be configured through the \texttt{FEATURES} register, which has the
following format:

\setlength{\regWidth}{.95\textwidth}
\begin{register}{H}{\texttt{FEATURES}}{0xF000\_0000}
  \regfield[gray!20]{reserved}{61}{3}{{uninitialized}}%
  \regfield{ctxsw}{1}{2}{0}%
  \regfield{vm}{1}{1}{0}%
  \regfield{kernel}{1}{0}{1}%
  \reglabel{Reset}\regnewline%
  \begin{regdesc}\begin{reglist}
    \item[ctxsw] whether the context-switching extension is available
    \item[vm] whether the virtual-memory extension is available
    \item[kernel] whether the TCU belongs to a kernel tile
  \end{reglist}\end{regdesc}
\end{register}
\setlength{\regWidth}{\textwidth}

\noindent At reset, \texttt{FEATURES.kernel} is set to 1, so that all tiles are kernel tiles. The
software starting on one tile can afterwards downgrade the other tiles to user tiles. If
\texttt{FEATURES.kernel} is 1, the CU can write to endpoint registers. This can be used to create a
communication channel to the TCU's MMIO region in another tile, which allows to establish other
communication channels by writing to the endpoint registers within the MMIO region.

The bits \texttt{FEATURES.ctxsw} and \texttt{FEATURES.vm} control whether the context-switching and
virtual-memory extension is enabled, respectively. If the former is disabled, foreign message
receptions do not raise a core request. If the latter is disabled, no address translation is
performed and thus no translation core request is raised. The behavior of privileged commands is
undefined if the associated extension is disabled.

Additionally, the TCU supports \emph{external commands}, which are triggered by writing to the
\texttt{EXT\_CMD} register and feedback is given via this register as well. The \texttt{EXT\_CMD}
register has the following format:

\begin{register}{H}{EXT\_CMD}{\texttt{0xF000\_0008}}
  \regfieldb{arg}{55}{9}%
  \regfieldb{err}{5}{4}%
  \regfieldb{op}{4}{0}\regnewline%
  \begin{regdesc}\begin{reglist}
    \item[arg] an argument for the operation
    \item[err] the result of the operation (output field)
    \item[op] the operation to execute
  \end{reglist}\end{regdesc}
\end{register}

\section{Command List}

The TCU supports the following external commands with the opcodes in parentheses. Other opcodes lead
to an \texttt{UNKNOWN\_CMD} error.

\begin{itemize}
  \item \texttt{IDLE} (0): don't do anything,
  \item \texttt{INV\_EP} (1): invalidate an endpoint.
\end{itemize}

\section{Pseudo Code Building Blocks}
\label{sec:extcmdspseudo}

The following sections use pseudo code to describe the behavior of the external TCU commands, based
on the following building blocks:

\begin{itemize}
  \item \texttt{ext\_stop(error)}:\\
  stop the execution of the external command by setting the opcode in \texttt{EXT\_CMD.op} to 0 and
  \texttt{EXT\_CMD.err} to the given error.
\end{itemize}

\section{Command Description}

\subsection{\texttt{INV\_EP}}

\begin{algorithm}[H]
    $epid \gets EXT\_CMD.arg\ \&\ 0xFFFF$\;
    $force \gets EXT\_CMD.arg >> 16$\;
    \BlankLine
    $EXT\_CMD.arg \gets 0$\;
    \BlankLine
    $ep \gets$ read\_ep(epid)\;
    \uIf{force == 0 and ep.type == SEND}{
      \uIf{ep.cur\_crd != ep.max\_crd}{ext\_stop(NO\_CREDITS)}
    }
    \uIf{force == 0 and ep.type == RECEIVE}{
      $EXT\_CMD.arg \gets ep.unread$\;
    }
    \BlankLine
    $ep \gets \{~type \gets INVALID~\}$\;
    $write\_ep(epid, ep)$\;
    \BlankLine
    $EXT\_CMD.err \gets NONE$\;
    $EXT\_CMD.op \gets 0$\;
    \caption{The TCU's \texttt{INV\_EP} command.}
\end{algorithm}
